Source: mobian-tweaks
Section: misc
Priority: optional
Maintainer: Arnaud Ferraris <aferraris@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Homepage: https://salsa.debian.org/Mobian-team/mobian-tweaks
Vcs-Git: https://salsa.debian.org/Mobian-team/mobian-tweaks.git
Vcs-Browser: https://salsa.debian.org/Mobian-team/mobian-tweaks
Rules-Requires-Root: no

Package: mobian-tweaks-common
Architecture: all
Depends: libglib2.0-bin,
         libpam-runtime,
         mobile-tweaks-common (>= 8),
         ${misc:Depends}
Description: Mobian -- Generic configuration settings
 Mobian is a Debian blend targeting mobile devices, such as phones and tablets.
 .
 This package provides device-independent configuration files aimed at improving
 the user experience on mobile devices:
   * PAM config for allowing numeric passwords
   * journald config for limiting the amount of disk space used by logs

Package: mobian-tweaks-phosh
Architecture: all
Depends: adduser,
         dconf-cli,
         feedbackd,
         mobian-tweaks-common (= ${binary:Version}),
         ${misc:Depends}
Description: Mobian -- Configuration settings for Phosh
 Mobian is a Debian blend targeting mobile devices, such as phones and tablets.
 .
 This package provides configuration files for the Phosh environment:
   * default GNOME settings more suited to mobile devices
